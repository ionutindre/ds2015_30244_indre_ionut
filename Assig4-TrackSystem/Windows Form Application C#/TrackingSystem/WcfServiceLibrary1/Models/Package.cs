﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WcfServiceLibrary1.Models
{
   public class Package
    {
        public int id { get; set; }
        public string sender { get; set; }
        public string receiver { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string sendercity { get; set; }
        public string destcity { get; set; }
        public int istracked { get; set; }
    }
}

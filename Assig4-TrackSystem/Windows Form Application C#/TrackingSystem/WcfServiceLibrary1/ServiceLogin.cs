﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MySql.Data.MySqlClient;
using WcfServiceLibrary1.Models;

namespace WcfServiceLibrary1
{ 
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ServiceLogin : IService
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public string LoginCheck(string usernameParsed, string passwordParsed)
        {
            // return string.Format("You entered: {0} {1}", username, pasword);
            string role = "";
            bool succesLogin = false;
            //sql connection
            server = "localhost";
            database = "trackingsystem";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);

            try
            {
                connection.Close();
                connection.Open();
               
            }
            catch (MySqlException ex)
            {
            }

            //string query = "SELECT FROM users where username=`admin`";
            string query = "SELECT * FROM users";
            //Create a list to store the result
            
            List<User> listUser = new List<User>();
           

            //Open connection
            //connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    User newUser = new User();
                    newUser.id = (int)dataReader["id"];
                    newUser.username = (string)dataReader["username"] as string;
                    newUser.password = dataReader["password"] as string;
                    newUser.role = (string)dataReader["role"] as string;
                    listUser.Add(newUser);
                    if (newUser.username == usernameParsed && newUser.password == passwordParsed) { succesLogin = true;role = newUser.role; }
            }

                //close Data Reader
                dataReader.Close();

            //close Connection
            connection.Close();

            return role;
        }

        public List<Package> GetAllPackages() {
           
            //sql connection
            server = "localhost";
            database = "trackingsystem";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);

            try
            {
                connection.Close();
                connection.Open();

            }
            catch (MySqlException ex)
            {
            }

            //string query = "SELECT FROM users where username=`admin`";
            string query = "SELECT * FROM package";
            //Create a list to store the result

            List<Package> listPackages = new List<Package>();


            //Open connection
            //connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(query, connection);
            //Create a data reader and Execute the command
            MySqlDataReader dataReader = cmd.ExecuteReader();

            //Read the data and store them in the list
            while (dataReader.Read())
            {
                Package newPKG = new Package();
                newPKG.id = (int)dataReader["id"];
                newPKG.sender = (string)dataReader["sender"] as string;
                newPKG.receiver = dataReader["receiver"] as string;
                newPKG.name = (string)dataReader["name"] as string;
                newPKG.sendercity = (string)dataReader["sendercity"] as string;
                newPKG.destcity = (string)dataReader["destcity"] as string;
                newPKG.description = (string)dataReader["description"] as string;
                newPKG.istracked = (int)dataReader["istracked"];
               
                listPackages.Add(newPKG);
            }

            //close Data Reader
            dataReader.Close();

            //close Connection
            connection.Close();

            return listPackages;
        }


        public Package GetPackageByID(int id)
        {

            //sql connection
            server = "localhost";
            database = "trackingsystem";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);

            try
            {
                connection.Close();
                connection.Open();

            }
            catch (MySqlException ex)
            {
            }

            //string query = "SELECT FROM users where username=`admin`";
            string query = "SELECT * FROM package where id=@id";
            //Create a list to store the result

            List<Package> listPackages = new List<Package>();


            //Open connection
            //connection.Open();
            //Create Command

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("id", id);
            //Create a data reader and Execute the command
            MySqlDataReader dataReader = cmd.ExecuteReader();

            //Read the data and store them in the list
            while (dataReader.Read())
            {
                Package newPKG = new Package();
                newPKG.id = (int)dataReader["id"];
                newPKG.sender = (string)dataReader["sender"] as string;
                newPKG.receiver = dataReader["receiver"] as string;
                newPKG.name = (string)dataReader["name"] as string;
                newPKG.sendercity = (string)dataReader["sendercity"] as string;
                newPKG.destcity = (string)dataReader["destcity"] as string;
                newPKG.description = (string)dataReader["description"] as string;
                newPKG.istracked = (int)dataReader["istracked"];

                listPackages.Add(newPKG);
            }

            //close Data Reader
            dataReader.Close();

            //close Connection
            connection.Close();

            return listPackages[0];
        }

        public List<Register> GetPackageStatusByID(int id)
        {

            //sql connection
            server = "localhost";
            database = "trackingsystem";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);

            try
            {
                connection.Close();
                connection.Open();

            }
            catch (MySqlException ex)
            {
            }

            //string query = "SELECT FROM users where username=`admin`";
            string query = "SELECT * FROM registerpackage";
            //Create a list to store the result

            List<Register> listPackages = new List<Register>();


            //Open connection
            //connection.Open();
            //Create Command

            MySqlCommand cmd = new MySqlCommand(query, connection);
           // cmd.Parameters.AddWithValue("id", id);
            //Create a data reader and Execute the command
            MySqlDataReader dataReader = cmd.ExecuteReader();

            //Read the data and store them in the list
            while (dataReader.Read())
            {
                Register newPKG = new Register();
                newPKG.id = (int)dataReader["id"];
                newPKG.idPkg = (int)dataReader["idpackage"];
                newPKG.cityname = dataReader["cityname"] as string;
                newPKG.date = (DateTime)dataReader["date"];
                if (newPKG.idPkg == id) {
                    listPackages.Add(newPKG);
                }
                
            }

            //close Data Reader
            dataReader.Close();

            //close Connection
            connection.Close();

            return listPackages;
        }

        public void RegisterUser(string username, string password, string role)
        {

            //sql connection
            server = "localhost";
            database = "trackingsystem";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);

            try
            {
                connection.Close();
                connection.Open();

            }
            catch (MySqlException ex)
            {
            }

            //string query = "SELECT FROM users where username=`admin`";
            string query = "INSERT INTO users (username,password,role) VALUES(@username,@password,@role)";

            //Open connection
            //connection.Open();
            //Create Command

            MySqlCommand cmd = new MySqlCommand(query, connection);

            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@role", role);
            // cmd.Parameters.AddWithValue("id", id);
            //Create a data reader and Execute the command
            MySqlDataReader dataReader = cmd.ExecuteReader();

            //Read the data and store them in the list
           

            //close Data Reader
            dataReader.Close();

            //close Connection
            connection.Close();

          
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}

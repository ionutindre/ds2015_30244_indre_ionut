﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsTrackingSystem
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            localhost.TrackingServicesService javaService = new localhost.TrackingServicesService();
            javaService.addPackage(textBox1.Text,textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text,"0");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            localhost.TrackingServicesService javaService = new localhost.TrackingServicesService();
            javaService.removePackage(textBox7.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            localhost.TrackingServicesService javaService = new localhost.TrackingServicesService();
            javaService.updatePackageStatus(textBox8.Text,textBox9.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            localhost.TrackingServicesService javaService = new localhost.TrackingServicesService();
            javaService.registerPackage(textBox11.Text,textBox10.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //REGISTER NEW USER
            ServiceReference1.ServiceClient client = new ServiceReference1.ServiceClient();
            client.RegisterUser(textBox12.Text,textBox13.Text,textBox14.Text);
        }
    }
}

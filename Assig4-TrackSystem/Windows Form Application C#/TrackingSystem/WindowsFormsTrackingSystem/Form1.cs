﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WcfServiceLibrary1.Models;

namespace WindowsFormsTrackingSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServiceReference1.ServiceClient client = new ServiceReference1.ServiceClient();
          //  string returnString;
            
          //  returnString = client.GetData(4);
            string result = client.LoginCheck(textBox1.Text,textBox2.Text);
            if (result == "user")
            {
                //hide Login Panel
                label1.Hide();
                label2.Hide();
                textBox1.Hide();
                textBox2.Hide();
                button1.Hide();

                //show user panel
                textBox3.Show();
                textBox4.Show();
                button2.Show();
                button4.Show();
                button3.Show();
                richTextBox1.Show();
            }
            if (result == "admin")
            {
                //localhost.TrackingServicesService javaService = new localhost.TrackingServicesService();
                //hide Login Panel
                label1.Hide();
                label2.Hide();
                textBox1.Hide();
                textBox2.Hide();
                button1.Hide();
                Form2 f2 = new Form2();
                f2.Show();
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            ServiceReference1.ServiceClient client = new ServiceReference1.ServiceClient();
           var  result = client.GetAllPackages();
            richTextBox1.Text = "";
            string strResult = "";
            foreach (var item in result)
            {
                string line = "ID: " + item.id + " NAME: "+ item.name + " SENDER: "+ 
                    item.sender +" RECEIVER: " + item.receiver + " DESC: "+ item.description +
                   " SNDCITY: " + item.sendercity + " DSTCITY: "+ item.destcity +" TRACK:  "+ item.istracked+ "\n";
                strResult += line + "\n";
            }
            richTextBox1.Text = strResult;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ServiceReference1.ServiceClient client = new ServiceReference1.ServiceClient();
            int id = Int32.Parse( textBox4.Text);
            var result = client.GetPackageStatusByID(id);
            richTextBox1.Text = "";
            string strResult = "";
            foreach (var item in result)
            {
                string line = "ID: " + item.id + " IDPKG: " + item.idPkg +
                    " CITY NAME: "+ item.cityname + " DATE: " +item.date +"\n";
                strResult += line + "\n";
            }
            richTextBox1.Text = strResult;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ServiceReference1.ServiceClient client = new ServiceReference1.ServiceClient();
            int id = Int32.Parse(textBox3.Text);
            var item = client.GetPackageByID(id);
            richTextBox1.Text = "";
            string strResult = "";

            string line = "ID: " + item.id + " NAME: " + item.name + " SENDER: " +
                item.sender + " RECEIVER: " + item.receiver + " DESC: " + item.description +
               " SNDCITY: " + item.sendercity + " DSTCITY: " + item.destcity + " TRACK:  " + item.istracked + "\n";
            strResult += line + "\n";

            richTextBox1.Text = strResult;
        }
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}

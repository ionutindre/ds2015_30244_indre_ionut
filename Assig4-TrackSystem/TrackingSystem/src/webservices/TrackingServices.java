package webservices;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.util.ArrayList;

////////////////////////////////////////////////////////////////////////////////
///WSDL REFERENCE SERVER SERVICE
//http://localhost:8080/TrackingSystem/services/TrackingServices?wsdl 
////////////////////////////////////////////////////////////////////////////////

public class TrackingServices {
	private Connection conn;
	
	public String addPackage(String sender, String receiver, String name, String description, String senderCity,
			String destCity, String isTracked){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 		
		
		try {
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/trackingsystem?" + "user=root&password=root");
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		try {

			String statement = "INSERT INTO `package` (`sender`, `receiver`, `name`, `description`, `sendercity`, `destcity`, `istracked`) VALUES (?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement dbStatement = conn.prepareStatement(statement);
			dbStatement.setString(1,sender);
			dbStatement.setString(2,receiver);
			dbStatement.setString(3,name);
			dbStatement.setString(4,description);
			dbStatement.setString(5,senderCity);
			dbStatement.setString(6,destCity);
			dbStatement.setInt(7,0);
			
			dbStatement.executeUpdate();
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
				
		return ("Added Package!") ;
	}
	
	public String removePackage(String packageId){
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 		
		
		try {
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/trackingsystem?" + "user=root&password=root");
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		try {

			String statement = "DELETE FROM `package` where id=?";
			PreparedStatement dbStatement = conn.prepareStatement(statement);
			dbStatement.setInt(1,Integer.parseInt(packageId));
			
			dbStatement.executeUpdate();
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		
		return ("Removed Package!");
	}
	
	public String updatePackageStatus(String packageId, String newCity){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 		
		
		try {
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/trackingsystem?" + "user=root&password=root");
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		
		// get current time here
		java.util.Calendar cal = java.util.Calendar.getInstance();
		java.util.Date utilDate = cal.getTime();
		java.sql.Date sqlDate = new Date(utilDate.getTime());
		
		try{
			String statement = "INSERT INTO `registerpackage` (`idpackage`, `cityname`, `date`) VALUES (?, ?, ?)";
			PreparedStatement dbStatement = conn.prepareStatement(statement);
			dbStatement.setInt(1,Integer.parseInt(packageId));
			dbStatement.setString(2,newCity);
			dbStatement.setDate(3,sqlDate);
			
			dbStatement.executeUpdate();
			}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		
		return ("Update Status for Package!");
	}
	
	public String registerPackage(String packageId, String currentCity){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 		
		
		try {
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/trackingsystem?" + "user=root&password=root");
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		
		// get current time here
	
	 int id = Integer.parseInt(packageId);
	 System.out.println(id);
		try{
			String statement = "UPDATE `package` SET `istracked`=? where `id`=?";
			PreparedStatement dbStatement = conn.prepareStatement(statement);
			dbStatement.setInt(1,1);
			dbStatement.setInt(2,id);
			dbStatement.executeUpdate();
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		
		try{
			java.util.Calendar cal = java.util.Calendar.getInstance();
			java.util.Date utilDate = cal.getTime();
			java.sql.Date sqlDate = new Date(utilDate.getTime());
			
			String statement = "INSERT INTO `registerpackage` (`idpackage`, `cityname`, `date`) VALUES (?, ?, ?)";
			PreparedStatement dbStatement = conn.prepareStatement(statement);
			dbStatement.setInt(1,Integer.parseInt(packageId));
			dbStatement.setString(2,currentCity);
			dbStatement.setDate(3,sqlDate);
			
			dbStatement.executeUpdate();
			}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		
		return ("Registered Package for Tracking!");
	}
}

/**
 * TrackingServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package webservices;

public interface TrackingServices extends java.rmi.Remote {
    public java.lang.String updatePackageStatus(java.lang.String packageId, java.lang.String newCity) throws java.rmi.RemoteException;
    public java.lang.String removePackage(java.lang.String packageId) throws java.rmi.RemoteException;
    public java.lang.String addPackage(java.lang.String sender, java.lang.String receiver, java.lang.String name, java.lang.String desc, java.lang.String senderCity, java.lang.String destCity, java.lang.String isTracked) throws java.rmi.RemoteException;
    public java.lang.String registerPackage(java.lang.String packageId, java.lang.String currentCity) throws java.rmi.RemoteException;
}

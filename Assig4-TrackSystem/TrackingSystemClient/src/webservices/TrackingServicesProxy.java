package webservices;

public class TrackingServicesProxy implements webservices.TrackingServices {
  private String _endpoint = null;
  private webservices.TrackingServices trackingServices = null;
  
  public TrackingServicesProxy() {
    _initTrackingServicesProxy();
  }
  
  public TrackingServicesProxy(String endpoint) {
    _endpoint = endpoint;
    _initTrackingServicesProxy();
  }
  
  private void _initTrackingServicesProxy() {
    try {
      trackingServices = (new webservices.TrackingServicesServiceLocator()).getTrackingServices();
      if (trackingServices != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)trackingServices)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)trackingServices)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (trackingServices != null)
      ((javax.xml.rpc.Stub)trackingServices)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public webservices.TrackingServices getTrackingServices() {
    if (trackingServices == null)
      _initTrackingServicesProxy();
    return trackingServices;
  }
  
  public java.lang.String updatePackageStatus(java.lang.String packageId, java.lang.String newCity) throws java.rmi.RemoteException{
    if (trackingServices == null)
      _initTrackingServicesProxy();
    return trackingServices.updatePackageStatus(packageId, newCity);
  }
  
  public java.lang.String removePackage(java.lang.String packageId) throws java.rmi.RemoteException{
    if (trackingServices == null)
      _initTrackingServicesProxy();
    return trackingServices.removePackage(packageId);
  }
  
  public java.lang.String addPackage(java.lang.String sender, java.lang.String receiver, java.lang.String name, java.lang.String desc, java.lang.String senderCity, java.lang.String destCity, java.lang.String isTracked) throws java.rmi.RemoteException{
    if (trackingServices == null)
      _initTrackingServicesProxy();
    return trackingServices.addPackage(sender, receiver, name, desc, senderCity, destCity, isTracked);
  }
  
  public java.lang.String registerPackage(java.lang.String packageId, java.lang.String currentCity) throws java.rmi.RemoteException{
    if (trackingServices == null)
      _initTrackingServicesProxy();
    return trackingServices.registerPackage(packageId, currentCity);
  }
  
  
}
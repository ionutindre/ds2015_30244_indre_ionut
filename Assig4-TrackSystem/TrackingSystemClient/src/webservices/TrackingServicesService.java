/**
 * TrackingServicesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package webservices;

public interface TrackingServicesService extends javax.xml.rpc.Service {
    public java.lang.String getTrackingServicesAddress();

    public webservices.TrackingServices getTrackingServices() throws javax.xml.rpc.ServiceException;

    public webservices.TrackingServices getTrackingServices(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

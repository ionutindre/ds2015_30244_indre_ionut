/**
 * TrackingServicesServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package webservices;

public class TrackingServicesServiceLocator extends org.apache.axis.client.Service implements webservices.TrackingServicesService {

    public TrackingServicesServiceLocator() {
    }


    public TrackingServicesServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TrackingServicesServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TrackingServices
    private java.lang.String TrackingServices_address = "http://localhost:8080/TrackingSystem/services/TrackingServices";

    public java.lang.String getTrackingServicesAddress() {
        return TrackingServices_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TrackingServicesWSDDServiceName = "TrackingServices";

    public java.lang.String getTrackingServicesWSDDServiceName() {
        return TrackingServicesWSDDServiceName;
    }

    public void setTrackingServicesWSDDServiceName(java.lang.String name) {
        TrackingServicesWSDDServiceName = name;
    }

    public webservices.TrackingServices getTrackingServices() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TrackingServices_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTrackingServices(endpoint);
    }

    public webservices.TrackingServices getTrackingServices(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            webservices.TrackingServicesSoapBindingStub _stub = new webservices.TrackingServicesSoapBindingStub(portAddress, this);
            _stub.setPortName(getTrackingServicesWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTrackingServicesEndpointAddress(java.lang.String address) {
        TrackingServices_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (webservices.TrackingServices.class.isAssignableFrom(serviceEndpointInterface)) {
                webservices.TrackingServicesSoapBindingStub _stub = new webservices.TrackingServicesSoapBindingStub(new java.net.URL(TrackingServices_address), this);
                _stub.setPortName(getTrackingServicesWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TrackingServices".equals(inputPortName)) {
            return getTrackingServices();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices", "TrackingServicesService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices", "TrackingServices"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TrackingServices".equals(portName)) {
            setTrackingServicesEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleTrackingServicesProxyid" scope="session" class="webservices.TrackingServicesProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleTrackingServicesProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleTrackingServicesProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleTrackingServicesProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        webservices.TrackingServices getTrackingServices10mtemp = sampleTrackingServicesProxyid.getTrackingServices();
if(getTrackingServices10mtemp == null){
%>
<%=getTrackingServices10mtemp %>
<%
}else{
        if(getTrackingServices10mtemp!= null){
        String tempreturnp11 = getTrackingServices10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String packageId_1id=  request.getParameter("packageId16");
            java.lang.String packageId_1idTemp = null;
        if(!packageId_1id.equals("")){
         packageId_1idTemp  = packageId_1id;
        }
        String newCity_2id=  request.getParameter("newCity18");
            java.lang.String newCity_2idTemp = null;
        if(!newCity_2id.equals("")){
         newCity_2idTemp  = newCity_2id;
        }
        java.lang.String updatePackageStatus13mtemp = sampleTrackingServicesProxyid.updatePackageStatus(packageId_1idTemp,newCity_2idTemp);
if(updatePackageStatus13mtemp == null){
%>
<%=updatePackageStatus13mtemp %>
<%
}else{
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(updatePackageStatus13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
}
break;
case 20:
        gotMethod = true;
        String packageId_3id=  request.getParameter("packageId23");
            java.lang.String packageId_3idTemp = null;
        if(!packageId_3id.equals("")){
         packageId_3idTemp  = packageId_3id;
        }
        java.lang.String removePackage20mtemp = sampleTrackingServicesProxyid.removePackage(packageId_3idTemp);
if(removePackage20mtemp == null){
%>
<%=removePackage20mtemp %>
<%
}else{
        String tempResultreturnp21 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(removePackage20mtemp));
        %>
        <%= tempResultreturnp21 %>
        <%
}
break;
case 25:
        gotMethod = true;
        String sender_4id=  request.getParameter("sender28");
            java.lang.String sender_4idTemp = null;
        if(!sender_4id.equals("")){
         sender_4idTemp  = sender_4id;
        }
        String receiver_5id=  request.getParameter("receiver30");
            java.lang.String receiver_5idTemp = null;
        if(!receiver_5id.equals("")){
         receiver_5idTemp  = receiver_5id;
        }
        String name_6id=  request.getParameter("name32");
            java.lang.String name_6idTemp = null;
        if(!name_6id.equals("")){
         name_6idTemp  = name_6id;
        }
        String desc_7id=  request.getParameter("desc34");
            java.lang.String desc_7idTemp = null;
        if(!desc_7id.equals("")){
         desc_7idTemp  = desc_7id;
        }
        String senderCity_8id=  request.getParameter("senderCity36");
            java.lang.String senderCity_8idTemp = null;
        if(!senderCity_8id.equals("")){
         senderCity_8idTemp  = senderCity_8id;
        }
        String destCity_9id=  request.getParameter("destCity38");
            java.lang.String destCity_9idTemp = null;
        if(!destCity_9id.equals("")){
         destCity_9idTemp  = destCity_9id;
        }
        String isTracked_10id=  request.getParameter("isTracked40");
            java.lang.String isTracked_10idTemp = null;
        if(!isTracked_10id.equals("")){
         isTracked_10idTemp  = isTracked_10id;
        }
        java.lang.String addPackage25mtemp = sampleTrackingServicesProxyid.addPackage(sender_4idTemp,receiver_5idTemp,name_6idTemp,desc_7idTemp,senderCity_8idTemp,destCity_9idTemp,isTracked_10idTemp);
if(addPackage25mtemp == null){
%>
<%=addPackage25mtemp %>
<%
}else{
        String tempResultreturnp26 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(addPackage25mtemp));
        %>
        <%= tempResultreturnp26 %>
        <%
}
break;
case 42:
        gotMethod = true;
        String packageId_11id=  request.getParameter("packageId45");
            java.lang.String packageId_11idTemp = null;
        if(!packageId_11id.equals("")){
         packageId_11idTemp  = packageId_11id;
        }
        String currentCity_12id=  request.getParameter("currentCity47");
            java.lang.String currentCity_12idTemp = null;
        if(!currentCity_12id.equals("")){
         currentCity_12idTemp  = currentCity_12id;
        }
        java.lang.String registerPackage42mtemp = sampleTrackingServicesProxyid.registerPackage(packageId_11idTemp,currentCity_12idTemp);
if(registerPackage42mtemp == null){
%>
<%=registerPackage42mtemp %>
<%
}else{
        String tempResultreturnp43 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(registerPackage42mtemp));
        %>
        <%= tempResultreturnp43 %>
        <%
}
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>
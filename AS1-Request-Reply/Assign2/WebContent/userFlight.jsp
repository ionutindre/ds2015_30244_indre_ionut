<%@page import="java.util.List"%>
<%@page import="org.app.service.FlightService"%>
<%@page import="java.util.Date"%>
<%@page import="org.app.model.Flight"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <title>Home Page</title>        
    </head>
    <body>  
        <div id="mystyle">
               
            <table>
                <thead>
                    <tr>
                        <th>Flight ID</th>
                        <th>Flight Number</th>
                        <th>Flight AirplaneType</th>
                        <th>Departure City</th>
                        <th>Departure Date</th>
                        <th>Arrival City</th>
                        <th>Arival Date</th>   
                        <th> ACTION </th>                     
                    </tr>
                </thead>
                <tbody>
                    <%
                        FlightService flightService = new FlightService();
                        List<Flight> list = flightService.getListOfFlights();
                        for (Flight u : list) {
                    %>
                    <tr>
                        <td><%=u.getId()%></td>
                        <td><%=u.getFlightNumber()%></td>
                        <td><%=u.getAirplaneType()%></td>
                        <td><%=u.getDepartureCity()%></td>
                        <td><%=u.getDepartureDate()%></td>
                        <td><%=u.getArrivalCity()%></td>
                        <td><%=u.getArrivalDate()%></td>
                      
                    </tr>
                    <%}%>
                <tbody>
            </table>            
            <br/>
        </div>   
    
    <div id="mystyle" class="myform" style="width:97.5%;">
        
            <form id="form" name="form" method="post" action="FlightServlet?action=localSearch">
                <h1>Search Flights By City and Automatic Get local time</h1>
                <p>Please enter the following information</p>

                <label>Arrival City
                    <span class="small">Ex : Cluj</span>
                </label>
                <input type="text" name="arrivalCity" id="arrivalCity" />
                <label>Departure City
                    <span class="small">Ex : Santa Monica</span>
                </label>
                <input type="text" name="departureCity" id="departureCity" />
                </br>
                <button type="submit" style="display:block;">Find Flight</button>
                <div class="spacer"></div>
    	     </form>
    </div>
    <br/>
    <a href="logout.jsp">Logout</a>
</body>
</html>

package org.app.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.app.model.City;
import org.app.model.Flight;
import org.app.model.User;
import org.app.service.CityService;
import org.app.service.FlightService;
import org.app.service.GeoApi;
import org.app.service.RegisterService;

/**
 * Servlet implementation class FlightServlet
 */
@WebServlet("/FlightServlet")
public class FlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        response.setContentType("text/html;charset=UTF-8");
	        String actionResult= request.getParameter("action");
	        System.out.println("FFFFF"+request.getSession().getAttribute("role"));
	      
	        if(!request.getSession().getAttribute("role").equals(null)){
	        	
	       
	       
	        if (actionResult.equals("delete")){
	        	 long flightId = Integer.parseInt(request.getParameter("flightId"));
	        	 FlightService flightService  = new FlightService();
	        	 boolean success = flightService.deleteFlight( flightId);
	        	 if (success == true)
	        	 {
	        		 PrintWriter out = response.getWriter();
	        		 out.println("Sucess!");

	        	 }
	        	 else {
	        		 PrintWriter out = response.getWriter();
	        		 out.println("Failed!");

	        	 }
	        }
	        
	        if (actionResult.equals("add")){
        		String airplaneType= request.getParameter("airplaneType");
        		String arrivalCity= request.getParameter("arrivalCity");
        		
        		String arrivalDate = request.getParameter("arrivalDate");
        		SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd hh:mm");
                Date parsedArrival = null;
				try {
					parsedArrival = format.parse(arrivalDate);
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
		        java.sql.Date sqlArrival = new java.sql.Date(parsedArrival.getTime());
			    		
        		String departureCity= request.getParameter("departureCity");
        		
        		String departureDate= request.getParameter("departureDate");
                Date parsedDeparture = null;
				try {
					parsedDeparture = format.parse(arrivalDate);
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
				 java.sql.Date sqlDeparture = new java.sql.Date(parsedDeparture.getTime());
        		
                int flightNumber= Integer.parseInt(request.getParameter("flightNumber"));
	        	 FlightService flightService  = new FlightService();
	        	 
	        	 Flight addFlight = new Flight(flightNumber, airplaneType, departureCity, sqlDeparture, arrivalCity, sqlArrival);
	        	 boolean success = flightService.addFlight( addFlight);
	        	 if (success == true)
	        	 {
	        		 PrintWriter out = response.getWriter();
	        		 out.println("Sucess!");

	        	 }
	        	 else {
	        		 PrintWriter out = response.getWriter();
	        		 out.println("Failed!");

	        	 }
	        }
	        
	        if (actionResult.equals("edit")){
	        	long flightId = Integer.parseInt(request.getParameter("flightId"));
	        	String airplaneType= request.getParameter("airplaneType");
        		String arrivalCity= request.getParameter("arrivalCity");
        		
        		String arrivalDate = request.getParameter("arrivalDate");
        		SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd hh:mm");
                Date parsedArrival = null;
				try {
					parsedArrival = format.parse(arrivalDate);
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
		        java.sql.Date sqlArrival = new java.sql.Date(parsedArrival.getTime());
			    		
        		String departureCity= request.getParameter("departureCity");
        		
        		String departureDate= request.getParameter("departureDate");
                Date parsedDeparture = null;
				try {
					parsedDeparture = format.parse(arrivalDate);
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
				 java.sql.Date sqlDeparture = new java.sql.Date(parsedDeparture.getTime());
        		
                int flightNumber= Integer.parseInt(request.getParameter("flightNumber"));
	        	 FlightService flightService  = new FlightService();
	        	 
	        	 Flight addFlight = new Flight(flightId,flightNumber, airplaneType, departureCity, sqlDeparture, arrivalCity, sqlArrival);
	        	 boolean success = flightService.editFlight( addFlight);
	        	 if (success == true)
	        	 {
	        		 PrintWriter out = response.getWriter();
	        		 out.println("Sucess!");

	        	 }
	        	 else {
	        		 PrintWriter out = response.getWriter();
	        		 out.println("Failed!");

	        	 }
	        }
	        
	        if (actionResult.equals("localSearch")){
	        	String arrivalCity= request.getParameter("arrivalCity");
	        	String departureCity= request.getParameter("departureCity");
	        	
	        	CityService cityService = new CityService();
	        	List<City> cities = new ArrayList<City>();
	        	cities = cityService.getCity(arrivalCity);
	        	Long longitude = cities.get(0).getLattitude();
	        	Long latitude =cities.get(0).getLongitude();
	        	//Geo Location Get Time
	        	GeoApi api = new GeoApi();
	        	Date retunedLocalDate = api.getLocalTimeByCoords(longitude.toString(), latitude.toString());
	        	
	        	FlightService flightService = new FlightService();
	        	List<Flight> flights = flightService.getListOfFlights();
	        	List<Flight> finalFlights = new ArrayList<Flight>();
	        	for ( Flight flight : flights) {
					if ((flight.getArrivalCity().equals(arrivalCity)&&(flight.getDepartureCity().equals(departureCity)))){
						if(flight.getArrivalDate().compareTo(retunedLocalDate)>0)
						{
							finalFlights.add(flight);
						}
							
					}
				}
	        	
	        	PrintWriter out = response.getWriter();
       		 	out.println(" <!DOCTYPE html>"+
       		"<html>"+
       		    
       		   " <body>  "+
       		     "  <div id='mystyle'>"+
       		          "  <table>"+
       		           "    <thead>"+
       		               "    <tr>"+
       		                 "      <th>Flight ID</th>"+
       		                   "     <th>Flight Number</th>"+
       		                   "     <th>Flight AirplaneType</th>"+
       		                 "      <th>Departure City</th>"+
       		                   "     <th>Departure Date</th>"+
       		                 "      <th>Arrival City</th>"+
       		                   "   <th>Arival Date</th>   "+   
       		                   " </tr>"+
       		               " </thead>"+
       		            "   <tbody>");
       		
       		 	for (Flight flight : finalFlights) {
       		 		out.println(
       		 	 "<tr>"+
                   " <td>"+flight.getId()+"</td>"+
                  "  <td>"+flight.getFlightNumber()+"</td>"+
                 "   <td>"+flight.getAirplaneType()+"</td>"+
                "    <td>"+flight.getDepartureCity()+"</td>"+
                 "   <td>"+flight.getDepartureDate()+"</td>"+
                 "   <td>"+flight.getArrivalCity()+"</td>"+
                 "   <td>"+flight.getArrivalDate()+"</td> "+
                "</tr>" );
				}
       		 	
       		                 out.println(          
       		              "  <tbody>"+
       		            "</table>  "+         
       		          "  <br/>"+
       		       " </div> ");
	        }
	        }
	    }

	
	  @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

	        processRequest(request, response);
	    }

	    @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }

	    @Override
	    public String getServletInfo() {
	        return "Flight description";
	    }

}

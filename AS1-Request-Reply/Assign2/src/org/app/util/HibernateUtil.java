package org.app.util;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
        	//Configuration configuration = new Configuration().configure();
        	//StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
        	//applySettings(configuration.getProperties());
           // sessionFactory = configuration.buildSessionFactory(builder.build());
        	//sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        
            // 1. configuring hibernate
			Configuration configuration = new Configuration().configure();

			// 2. create sessionfactory
		sessionFactory = configuration.buildSessionFactory();

			// 3. Get Session object
			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
			
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session openSession() {
        return sessionFactory.openSession();
    }
}
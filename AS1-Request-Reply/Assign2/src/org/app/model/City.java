package org.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="City")
public class City {
	
	@Id @GeneratedValue
    private Long id;
	private String Name;
	private long lattitude;
	private long longitude;
	
	public City(){
		
	}
	
	public City(String name, long lattitude, long longitude) {
		super();
		Name = name;
		this.lattitude = lattitude;
		this.longitude = longitude;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public long getLattitude() {
		return lattitude;
	}
	public void setLattitude(long lattitude) {
		this.lattitude = lattitude;
	}
	public long getLongitude() {
		return longitude;
	}
	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}
	
}

package org.app.model;

import java.sql.Date;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Flight")
public class Flight implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
    private Long id;
	private int flightNumber;
	private String airplaneType;
	private String departureCity;
	private Date departureDate;
	private String arrivalCity;
	private Date arrivalDate;
	
	public Flight(){
		
	}
	
	public Flight(Long id, int flightNumber, String airplaneType, String departureCity, Date departureDate,
			String arrivalCity, Date arrivalDate) {
		this.id = id;
		this.flightNumber = flightNumber;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.departureDate = departureDate;
		this.arrivalCity = arrivalCity;
		this.arrivalDate = arrivalDate;
	}
	
	public Flight(int flightNumber, String airplaneType, String departureCity, Date departureDate, String arrivalCity,
			Date arrivalDate) {
		this.flightNumber = flightNumber;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.departureDate = departureDate;
		this.arrivalCity = arrivalCity;
		this.arrivalDate = arrivalDate;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
}

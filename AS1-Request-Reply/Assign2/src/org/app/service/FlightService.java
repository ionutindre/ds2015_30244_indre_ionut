package org.app.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.app.model.Flight;
import org.app.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class FlightService {
	
	   public List<Flight> getListOfFlights(){
	        List<Flight> list = new ArrayList<Flight>();
			// 1. configuring hibernate
			Configuration configuration = new Configuration().configure();

			// 2. create sessionfactory
			SessionFactory sessionFactory = configuration.buildSessionFactory();

			// 3. Get Session object
			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
	        Transaction tx = null;        
	        try {
	            tx = session.getTransaction();
	            tx.begin();
	            list = session.createQuery("from Flight").list();                        
	            tx.commit();
	        } catch (Exception e) {
	            if (tx != null) {
	                tx.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }
	        return list;
	    }
	   
	   public boolean addFlight(Flight flight){
		
	        List<Flight> list = new ArrayList<Flight>();
			// 1. configuring hibernate
			Configuration configuration = new Configuration().configure();

			// 2. create sessionfactory
			SessionFactory sessionFactory = configuration.buildSessionFactory();

			// 3. Get Session object
			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
	        Transaction tx = null;  
	        boolean success = true;
	        try {
	            tx = session.getTransaction();
	            tx.begin();
	            session.saveOrUpdate(flight);            
	            tx.commit();
	        } catch (Exception e) {
	            if (tx != null) {
	                tx.rollback();
	                success = false;
	            }
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }        
	        return success;
	    }
	   
	   public boolean deleteFlight(long flightID){
			
	        List<Flight> list = new ArrayList<Flight>();
			// 1. configuring hibernate
			Configuration configuration = new Configuration().configure();

			// 2. create sessionfactory
			SessionFactory sessionFactory = configuration.buildSessionFactory();

			// 3. Get Session object
			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
	        Transaction tx = null;  
	        boolean success = true;
	        try {
	            tx = session.getTransaction();
	            tx.begin();
	            Query q = session.createQuery("delete Flight where id ='"+flightID+"'");
	            q.executeUpdate();
	            tx.commit();
	        } catch (Exception e) {
	            if (tx != null) {
	                tx.rollback();
	                success = false;
	            }
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }        
	        return success;
	    }
	   
	   public boolean editFlight(Flight flight){
			
	        List<Flight> list = new ArrayList<Flight>();
			// 1. configuring hibernate
			Configuration configuration = new Configuration().configure();

			// 2. create sessionfactory
			SessionFactory sessionFactory = configuration.buildSessionFactory();

			// 3. Get Session object
			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
	        Transaction tx = null;  
	        boolean success = true;
	        try {
	            tx = session.getTransaction();
	            tx.begin();
	            Flight flightGET = 
	                    (Flight)session.load(Flight.class, flight.getId()); 
	            flightGET.setAirplaneType(flight.getAirplaneType());
	            flightGET.setArrivalCity(flight.getArrivalCity());
	            flightGET.setArrivalDate(flight.getArrivalDate());
	            flightGET.setDepartureCity(flight.getDepartureCity());
	            flightGET.setDepartureDate(flight.getDepartureDate());
	            flightGET.setFlightNumber(flight.getFlightNumber());
	            
	            session.save(flightGET); 
	         tx.commit();    
	            tx.commit();
	        } catch (Exception e) {
	            if (tx != null) {
	                tx.rollback();
	               // success = false;
	            }
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }        
	        return success;
	    }

	public List<Flight> getSpecificFlights(Date retunedLocalDate, String arrivalCity, String departureCity) {
		 List<Flight> list = new ArrayList<Flight>();
			// 1. configuring hibernate
			Configuration configuration = new Configuration().configure();

			// 2. create sessionfactory
			SessionFactory sessionFactory = configuration.buildSessionFactory();

			// 3. Get Session object
			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
	        Transaction tx = null;        
	        try {
	            tx = session.getTransaction();
	            tx.begin();
	            list = session.createQuery("from Flight where arrivalCity='"+arrivalCity+"'"+"departureCity='"+
	            departureCity+"'").list();                        
	            tx.commit();
	        } catch (Exception e) {
	            if (tx != null) {
	                tx.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }
	        return list;
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Messaging;
using System.Collections.Generic;
using MyQueue.Services;

namespace MyQueue
{
	/// <summary>
	/// Summary description for Form2.
	/// </summary>
	public class QueueProd : System.Windows.Forms.Form
	{
		
		private TextBox txtTitle;
        private TextBox textYear;
        private TextBox textPrice;

        private Label labelDVD;
        private Label labelTitle;
        private Label labelYear;
        private Label labelPrice;
        private Label labelMessage;
        private System.Windows.Forms.Button btnAdd;

		public System.Messaging.MessageQueue mq;
        public System.Messaging.MessageQueue emailQ;

        public static Int32 i = 0;
        private ListBox MsgBox;
        
        private FontDialog fontDialog;
        private List<string> emailList = new List<string>();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new QueueProd());
        }

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public QueueProd()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
              
            //const string QUEUE_PATH = @"formatname:MULTICAST=234.1.1.1:8001";
            //  mq = new MessageQueue(QUEUE_PATH);
			//Q Creation
            
            if (MessageQueue.Exists(@".\Private$\MyQueue"))
            {
                mq = new System.Messaging.MessageQueue(@".\Private$\MyQueue");
                mq.MulticastAddress = "234.1.1.1:8001";
              //  const string QUEUE_PATH = @"formatname:MULTICAST=234.1.1.1:8001";
             //   mq = new MessageQueue(QUEUE_PATH);
                MsgBox.Items.Add("Setting the queue.");
            }

            else 
            {
                mq = MessageQueue.Create(@".\Private$\MyQueue");
                mq.MulticastAddress = "234.1.1.1:8001";

              //  const string QUEUE_PATH = @"formatname:MULTICAST=234.1.1.1:8001";
              //  mq = new MessageQueue(QUEUE_PATH);
                
                MsgBox.Items.Add("Create the queue.");
            }


            if (MessageQueue.Exists(@".\Private$\EmailQ"))
            {
                emailQ = new System.Messaging.MessageQueue(@".\Private$\EmailQ");

            }

            else
            {
                emailQ = MessageQueue.Create(@".\Private$\EmailQ");

            }
             				
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelMessage = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.textYear = new System.Windows.Forms.TextBox();
            this.textPrice = new System.Windows.Forms.TextBox();
            this.labelDVD = new System.Windows.Forms.Label();
            this.MsgBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.Location = new System.Drawing.Point(12, 45);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(100, 23);
            this.labelTitle.TabIndex = 5;
            this.labelTitle.Text = "Title";
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(12, 158);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(160, 23);
            this.labelMessage.TabIndex = 4;
            this.labelMessage.Text = "Messages";
            // 
            // labelYear
            // 
            this.labelYear.Location = new System.Drawing.Point(12, 71);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(100, 23);
            this.labelYear.TabIndex = 8;
            this.labelYear.Text = "Year";
            // 
            // labelPrice
            // 
            this.labelPrice.Location = new System.Drawing.Point(12, 97);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(100, 23);
            this.labelPrice.TabIndex = 9;
            this.labelPrice.Text = "Price";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnAdd.Location = new System.Drawing.Point(301, 128);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 31);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(118, 41);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(278, 20);
            this.txtTitle.TabIndex = 6;
            // 
            // textYear
            // 
            this.textYear.Location = new System.Drawing.Point(118, 68);
            this.textYear.Name = "textYear";
            this.textYear.Size = new System.Drawing.Size(278, 20);
            this.textYear.TabIndex = 7;
            // 
            // textPrice
            // 
            this.textPrice.Location = new System.Drawing.Point(118, 97);
            this.textPrice.Name = "textPrice";
            this.textPrice.Size = new System.Drawing.Size(278, 20);
            this.textPrice.TabIndex = 10;
            // 
            // labelDVD
            // 
            this.labelDVD.AutoSize = true;
            this.labelDVD.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDVD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelDVD.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelDVD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelDVD.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelDVD.Location = new System.Drawing.Point(12, 9);
            this.labelDVD.Name = "labelDVD";
            this.labelDVD.Size = new System.Drawing.Size(135, 19);
            this.labelDVD.TabIndex = 11;
            this.labelDVD.Text = "DVD INPUT FIELDS";
            // 
            // MsgBox
            // 
            this.MsgBox.Location = new System.Drawing.Point(11, 184);
            this.MsgBox.Name = "MsgBox";
            this.MsgBox.Size = new System.Drawing.Size(392, 121);
            this.MsgBox.TabIndex = 2;
            // 
            // ServQueue
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(408, 317);
            this.ControlBox = false;
            this.Controls.Add(this.labelDVD);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.textPrice);
            this.Controls.Add(this.textYear);
            this.Controls.Add(this.MsgBox);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServQueue";
            this.Text = "Admin";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
            MsgBox.Items.Add("Sending new object message to the queue.");
           
			System.Messaging.Message mm = new System.Messaging.Message();
            //mm.Body = txtTitle.Text + textYear.Text + textPrice.Text;
            mm.Body = new DVD
            {
                Title = txtTitle.Text,
                Year = int.Parse(textYear.Text),
                Price = double.Parse(textPrice.Text)
            };
			mm.Label = "MsgI" + i.ToString();
			i++;
			mq.Send(mm);
            DVD sendDVD =(DVD)mm.Body;
            // Save new DVD to file
            StoreDVD.storeNewDVD(sendDVD);
            //

            //Read All Emails in Queue
            bool stopFlag = false;
            while (stopFlag == false)
            {
                try
                {
                    System.Messaging.Message emailMess = new System.Messaging.Message();
                    emailMess = emailQ.Receive(new TimeSpan(0, 0, 3));
                    emailMess.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
                    var recivedEmail = emailMess.Body;
                    MsgBox.Items.Add("NEW EMAIL : " + recivedEmail.ToString());
                    emailList.Add(recivedEmail.ToString());
                }
                catch
                {
                    MsgBox.Items.Add("No new Email!");
                    stopFlag = true;
                }
               
            }

            //Send Email to all Stored Email in Email List
            foreach (var emailTo in emailList)
            {
                SendEmail.sendEmail(emailTo,sendDVD);
            }
           

		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyQueue.Model
{
    [Serializable]
    public class DVD
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public double Price { get; set; }
    }
}

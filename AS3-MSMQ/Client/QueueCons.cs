using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Messaging;
using System.Threading;
using MyQueue.Model;

namespace MyQueue
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class QueueCons : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox txtMsg;
		private System.Windows.Forms.Button btnMsg;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox MsgBox;
		private System.Windows.Forms.Label Messages;
		private System.Windows.Forms.Button btnRcv;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		public System.Messaging.MessageQueue mq;
        public System.Messaging.MessageQueue emailQ;
		public static Int32 j=0;

		public QueueCons()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			//Q Creation
			if(MessageQueue.Exists(@".\Private$\MyQueue"))
            {
                mq = new System.Messaging.MessageQueue(@".\Private$\MyQueue");
                mq.MulticastAddress = "234.1.1.1:8001";
            }
				
			else
            {
                mq = MessageQueue.Create(@".\Private$\MyQueue");
                mq.MulticastAddress = "234.1.1.1:8001";
            }


            if (MessageQueue.Exists(@".\Private$\EmailQ"))
            {
                emailQ = new System.Messaging.MessageQueue(@".\Private$\EmailQ");
               
            }

            else
            {
                emailQ = MessageQueue.Create(@".\Private$\EmailQ");
               
            }
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.btnMsg = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.MsgBox = new System.Windows.Forms.ListBox();
            this.Messages = new System.Windows.Forms.Label();
            this.btnRcv = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtMsg
            // 
            this.txtMsg.Location = new System.Drawing.Point(12, 42);
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(280, 20);
            this.txtMsg.TabIndex = 0;
            // 
            // btnMsg
            // 
            this.btnMsg.Location = new System.Drawing.Point(321, 42);
            this.btnMsg.Name = "btnMsg";
            this.btnMsg.Size = new System.Drawing.Size(75, 23);
            this.btnMsg.TabIndex = 1;
            this.btnMsg.Text = "&Send";
            this.btnMsg.Click += new System.EventHandler(this.btnMsg_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Send Email:";
            // 
            // MsgBox
            // 
            this.MsgBox.Location = new System.Drawing.Point(8, 136);
            this.MsgBox.Name = "MsgBox";
            this.MsgBox.Size = new System.Drawing.Size(392, 173);
            this.MsgBox.TabIndex = 2;
            // 
            // Messages
            // 
            this.Messages.Location = new System.Drawing.Point(9, 104);
            this.Messages.Name = "Messages";
            this.Messages.Size = new System.Drawing.Size(160, 23);
            this.Messages.TabIndex = 3;
            this.Messages.Text = "Messages : ";
            // 
            // btnRcv
            // 
            this.btnRcv.Location = new System.Drawing.Point(321, 104);
            this.btnRcv.Name = "btnRcv";
            this.btnRcv.Size = new System.Drawing.Size(75, 23);
            this.btnRcv.TabIndex = 5;
            this.btnRcv.Text = "&Receive";
            this.btnRcv.Click += new System.EventHandler(this.btnRcv_Click);
            // 
            // QueueCons
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(408, 317);
            this.Controls.Add(this.btnRcv);
            this.Controls.Add(this.Messages);
            this.Controls.Add(this.MsgBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMsg);
            this.Controls.Add(this.txtMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QueueCons";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new QueueCons());
		}

		private void btnMsg_Click(object sender, System.EventArgs e)
		{
//			SendMessage(Handle, 1, 0, IntPtr.Zero);
			System.Messaging.Message mm = new System.Messaging.Message();
			mm.Body = txtMsg.Text;
			mm.Label = "Msg" + j.ToString();
			j++;
			emailQ.Send(mm);
		}

		private void btnRcv_Click(object sender, System.EventArgs e)
		{
			System.Messaging.Message mes;
			string m;

			try
			{
				mes = mq.Receive(new TimeSpan(0, 0, 3));
                mes.Formatter = new XmlMessageFormatter(new Type[] { typeof(DVD) });
                var recivedDVD = (DVD)mes.Body;
                m = "New DVD has been added "+"Title: "+recivedDVD.Title +" Year: "+ recivedDVD.Year.ToString() +" Price: "+ recivedDVD.Price.ToString(); 
			}
			catch
			{
				m = "No Message";
			}
			MsgBox.Items.Add(m.ToString());
		}

	}
}

﻿using System;
using System.Globalization;
using System.Windows.Forms;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using RemotableObjects;

namespace RClient
{
    public partial class ClientForm : Form, IObserver
    {
        private const int TcpPort = 8080;

        private readonly MyRemotableObject      _messageRemoteObject;
        private readonly TaxRemoteObject    _taxRemoteObject;
        private readonly SellingPriceRemoteObject  _priceRemoteObject;

        public ClientForm()
        {
            InitializeComponent();

            var tcpChannel = new TcpChannel();
            ChannelServices.RegisterChannel(tcpChannel, false);

            _messageRemoteObject    = (MyRemotableObject)Activator.GetObject(typeof(MyRemotableObject),         "tcp://localhost:" + TcpPort + "/MessageSending");
            _taxRemoteObject        = (TaxRemoteObject)Activator.GetObject(typeof(TaxRemoteObject),     "tcp://localhost:" + TcpPort + "/TaxCalculation");
            _priceRemoteObject      = (SellingPriceRemoteObject)Activator.GetObject(typeof(SellingPriceRemoteObject), "tcp://localhost:" + TcpPort + "/PriceCalculation");
        }
       
        public void Notify(string text)
        {
            throw new NotImplementedException();
        }

        private void btnTaxCalculator_Click(object sender, EventArgs e)
        {
            _messageRemoteObject.SetMessage("Now we are calculating the price of your tax!");

            var engineCapacity = Convert.ToInt32(txtBoxEngineCapacity.Text);
          
            txtBoxResult.Text = _taxRemoteObject.CalculateTax(engineCapacity).ToString(CultureInfo.InvariantCulture);
        }

        private void btnSellingPriceCalculator_Click(object sender, EventArgs e)
        {
            _messageRemoteObject.SetMessage("Now we are calculating the wright price for selling your car!");

            var fabYear = Convert.ToInt32(txtBoxFabYear.Text);
            var purchasePrice = Convert.ToDouble(txtBoxPurchasePrice.Text);

            txtBoxResult.Text = _priceRemoteObject.CalculateSelingPrice(purchasePrice, fabYear).ToString(CultureInfo.InvariantCulture);
        }

        private void gBoxTaxValue_Enter(object sender, EventArgs e)
        {

        }

        private void lblFabYear_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
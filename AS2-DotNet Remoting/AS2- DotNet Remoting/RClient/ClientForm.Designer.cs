﻿namespace RClient
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTaxCalculator = new System.Windows.Forms.Button();
            this.lblHeaderText = new System.Windows.Forms.Label();
            this.lblEngineCapacity = new System.Windows.Forms.Label();
            this.txtBoxEngineCapacity = new System.Windows.Forms.TextBox();
            this.txtBoxFabYear = new System.Windows.Forms.TextBox();
            this.txtBoxPurchasePrice = new System.Windows.Forms.TextBox();
            this.lblFabYear = new System.Windows.Forms.Label();
            this.lblPurchasePrice = new System.Windows.Forms.Label();
            this.btnSellingPriceCalculator = new System.Windows.Forms.Button();
            this.txtBoxResult = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTaxCalculator
            // 
            this.btnTaxCalculator.BackColor = System.Drawing.Color.DarkRed;
            this.btnTaxCalculator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTaxCalculator.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnTaxCalculator.Location = new System.Drawing.Point(452, 86);
            this.btnTaxCalculator.Margin = new System.Windows.Forms.Padding(4);
            this.btnTaxCalculator.Name = "btnTaxCalculator";
            this.btnTaxCalculator.Size = new System.Drawing.Size(158, 52);
            this.btnTaxCalculator.TabIndex = 0;
            this.btnTaxCalculator.Text = "Calculate Tax";
            this.btnTaxCalculator.UseVisualStyleBackColor = false;
            this.btnTaxCalculator.Click += new System.EventHandler(this.btnTaxCalculator_Click);
            // 
            // lblHeaderText
            // 
            this.lblHeaderText.AutoSize = true;
            this.lblHeaderText.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderText.Location = new System.Drawing.Point(106, 20);
            this.lblHeaderText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderText.Name = "lblHeaderText";
            this.lblHeaderText.Size = new System.Drawing.Size(342, 25);
            this.lblHeaderText.TabIndex = 1;
            this.lblHeaderText.Text = "Automobile Calculator: Tax and Prices";
            // 
            // lblEngineCapacity
            // 
            this.lblEngineCapacity.AutoSize = true;
            this.lblEngineCapacity.Location = new System.Drawing.Point(48, 89);
            this.lblEngineCapacity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEngineCapacity.Name = "lblEngineCapacity";
            this.lblEngineCapacity.Size = new System.Drawing.Size(104, 17);
            this.lblEngineCapacity.TabIndex = 2;
            this.lblEngineCapacity.Text = "Engine Capacity";
            // 
            // txtBoxEngineCapacity
            // 
            this.txtBoxEngineCapacity.Location = new System.Drawing.Point(250, 89);
            this.txtBoxEngineCapacity.Name = "txtBoxEngineCapacity";
            this.txtBoxEngineCapacity.Size = new System.Drawing.Size(156, 25);
            this.txtBoxEngineCapacity.TabIndex = 3;
            // 
            // txtBoxFabYear
            // 
            this.txtBoxFabYear.Location = new System.Drawing.Point(250, 204);
            this.txtBoxFabYear.Name = "txtBoxFabYear";
            this.txtBoxFabYear.Size = new System.Drawing.Size(156, 25);
            this.txtBoxFabYear.TabIndex = 5;
            // 
            // txtBoxPurchasePrice
            // 
            this.txtBoxPurchasePrice.Location = new System.Drawing.Point(250, 232);
            this.txtBoxPurchasePrice.Name = "txtBoxPurchasePrice";
            this.txtBoxPurchasePrice.Size = new System.Drawing.Size(156, 25);
            this.txtBoxPurchasePrice.TabIndex = 6;
            // 
            // lblFabYear
            // 
            this.lblFabYear.AutoSize = true;
            this.lblFabYear.Location = new System.Drawing.Point(47, 204);
            this.lblFabYear.Name = "lblFabYear";
            this.lblFabYear.Size = new System.Drawing.Size(105, 17);
            this.lblFabYear.TabIndex = 7;
            this.lblFabYear.Text = "Fabrication Year";
            this.lblFabYear.Click += new System.EventHandler(this.lblFabYear_Click);
            // 
            // lblPurchasePrice
            // 
            this.lblPurchasePrice.AutoSize = true;
            this.lblPurchasePrice.Location = new System.Drawing.Point(48, 232);
            this.lblPurchasePrice.Name = "lblPurchasePrice";
            this.lblPurchasePrice.Size = new System.Drawing.Size(108, 17);
            this.lblPurchasePrice.TabIndex = 8;
            this.lblPurchasePrice.Text = "Purchasing Price";
            // 
            // btnSellingPriceCalculator
            // 
            this.btnSellingPriceCalculator.BackColor = System.Drawing.Color.DarkRed;
            this.btnSellingPriceCalculator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSellingPriceCalculator.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSellingPriceCalculator.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnSellingPriceCalculator.Location = new System.Drawing.Point(452, 204);
            this.btnSellingPriceCalculator.Name = "btnSellingPriceCalculator";
            this.btnSellingPriceCalculator.Size = new System.Drawing.Size(158, 53);
            this.btnSellingPriceCalculator.TabIndex = 10;
            this.btnSellingPriceCalculator.Text = "Calculate Price";
            this.btnSellingPriceCalculator.UseVisualStyleBackColor = false;
            this.btnSellingPriceCalculator.Click += new System.EventHandler(this.btnSellingPriceCalculator_Click);
            // 
            // txtBoxResult
            // 
            this.txtBoxResult.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtBoxResult.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtBoxResult.Location = new System.Drawing.Point(23, 283);
            this.txtBoxResult.Multiline = true;
            this.txtBoxResult.Name = "txtBoxResult";
            this.txtBoxResult.ReadOnly = true;
            this.txtBoxResult.Size = new System.Drawing.Size(587, 47);
            this.txtBoxResult.TabIndex = 13;
            this.txtBoxResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.PaleGreen;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 23);
            this.label1.TabIndex = 14;
            this.label1.Text = "Tax Section";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.PaleGreen;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 23);
            this.label2.TabIndex = 15;
            this.label2.Text = "Selling Section";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(632, 342);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSellingPriceCalculator);
            this.Controls.Add(this.lblFabYear);
            this.Controls.Add(this.lblPurchasePrice);
            this.Controls.Add(this.txtBoxFabYear);
            this.Controls.Add(this.txtBoxPurchasePrice);
            this.Controls.Add(this.btnTaxCalculator);
            this.Controls.Add(this.txtBoxResult);
            this.Controls.Add(this.lblEngineCapacity);
            this.Controls.Add(this.txtBoxEngineCapacity);
            this.Controls.Add(this.lblHeaderText);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".NET Remoting - Remote Client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTaxCalculator;
        private System.Windows.Forms.Label lblHeaderText;
        private System.Windows.Forms.Label lblEngineCapacity;
        private System.Windows.Forms.TextBox txtBoxEngineCapacity;
        private System.Windows.Forms.TextBox txtBoxFabYear;
        private System.Windows.Forms.TextBox txtBoxPurchasePrice;
        private System.Windows.Forms.Label lblFabYear;
        private System.Windows.Forms.Label lblPurchasePrice;
        private System.Windows.Forms.Button btnSellingPriceCalculator;
        private System.Windows.Forms.TextBox txtBoxResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
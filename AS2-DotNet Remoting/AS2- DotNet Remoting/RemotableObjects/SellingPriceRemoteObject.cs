using System;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;

namespace RemotableObjects
{
    public class SellingPriceRemoteObject : MarshalByRefObject
	{
		public SellingPriceRemoteObject()
		{
		
		}

        public double CalculateSelingPrice(double purchasePrice, int fabricationYear)
        {
            var sellingPrice = (purchasePrice - (purchasePrice / 7) )* (2015 - fabricationYear);
            return sellingPrice;
        }
	}
}

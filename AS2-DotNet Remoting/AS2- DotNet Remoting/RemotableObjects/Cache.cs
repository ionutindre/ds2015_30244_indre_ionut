
namespace RemotableObjects
{
	public class Cache
	{
		private static Cache _myInstance;
		public static IObserver Observer;

		private Cache()
		{

		}
		
		public static void Attach(IObserver observer)
		{
			Observer = observer;
		}
		public static Cache GetInstance()
		{
		    return _myInstance ?? (_myInstance = new Cache());
		}

	    public string MessageString
		{
			set 
			{
				Observer.Notify(value);
			}
		}
	}
}
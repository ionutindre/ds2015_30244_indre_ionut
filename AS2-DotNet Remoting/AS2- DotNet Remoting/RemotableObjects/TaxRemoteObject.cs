using System;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;

namespace RemotableObjects
{
	public class TaxRemoteObject : MarshalByRefObject
	{
        public TaxRemoteObject()
		{
		
		}

        public double CalculateTax(int engineCapacity)
        {           
            var sum = 0;
            double calculatedTax = 0.0;

            if(engineCapacity > 0)
            {
                if (engineCapacity <= 1600)
                { 
                     sum = 8;
                    goto IDENTIFIED;
                }
                if (engineCapacity >= 1601 && engineCapacity <= 2000)
                {
                    sum = 18;
                    goto IDENTIFIED;
                }
                if (engineCapacity >= 2001 && engineCapacity <= 2600)
                {
                    sum = 72;
                    goto IDENTIFIED;
                }
                if (engineCapacity >= 2601 && engineCapacity <= 3000)
                {
                    sum = 144;
                    goto IDENTIFIED;
                }
                if (engineCapacity >= 3001)
                {
                    sum = 290;
                    goto IDENTIFIED;
                }
            }
            else
            {
               throw new Exception("Wrong Input Data on Engine Capacity!");
            }

            IDENTIFIED:
                calculatedTax = engineCapacity / 200 * sum;

            return calculatedTax;
        }
	}
}

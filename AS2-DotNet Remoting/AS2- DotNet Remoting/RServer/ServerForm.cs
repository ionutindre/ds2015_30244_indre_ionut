﻿using System;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using RemotableObjects;

namespace RServer
{
    public partial class ServerForm : Form, IObserver
    {
        private const int TcpPort = 8080;
        private delegate void SetTextCallback(string text);     

        public ServerForm()
        {
            InitializeComponent();
            
            statusTxtBox.Text += "Starting Server" + Environment.NewLine;
            statusTxtBox.Text += "Opening Channel" + Environment.NewLine;

            var tcpChannel = new TcpChannel(TcpPort);
            ChannelServices.RegisterChannel(tcpChannel, false);

            statusTxtBox.Text += "Listening On Channel "        + TcpPort   + Environment.NewLine;
            statusTxtBox.Text += "Posting remote services"               + Environment.NewLine;

            RemotingConfiguration.RegisterWellKnownServiceType(typeof(MyRemotableObject),     "MessageSending",     WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(TaxRemoteObject),   "TaxCalculation",     WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(SellingPriceRemoteObject), "PriceCalculation",   WellKnownObjectMode.Singleton);

            statusTxtBox.Text += "Server Listening" + Environment.NewLine;

            Cache.Attach(this);
        }

        private void SetText(string text)
        {
            statusTxtBox.Text += text + Environment.NewLine;
        }

        public void Notify(string text)
        {
            // InvokeRequired compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (statusTxtBox.InvokeRequired)
            {
                var delegateText = new SetTextCallback(SetText);
                Invoke(delegateText, text);
            }
            else
            {
                statusTxtBox.Text += text + Environment.NewLine;
            }
        }
    }
}
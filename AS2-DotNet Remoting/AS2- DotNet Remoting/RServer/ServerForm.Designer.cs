﻿namespace RServer
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusTxtBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // statusTxtBox
            // 
            this.statusTxtBox.BackColor = System.Drawing.Color.White;
            this.statusTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusTxtBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusTxtBox.Enabled = false;
            this.statusTxtBox.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusTxtBox.Location = new System.Drawing.Point(0, 0);
            this.statusTxtBox.Multiline = true;
            this.statusTxtBox.Name = "statusTxtBox";
            this.statusTxtBox.ReadOnly = true;
            this.statusTxtBox.Size = new System.Drawing.Size(451, 246);
            this.statusTxtBox.TabIndex = 1;
            // 
            // ServerForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 18);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(451, 246);
            this.Controls.Add(this.statusTxtBox);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ServerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".NET Remoting - Remote Server";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.TextBox statusTxtBox;
    }
}